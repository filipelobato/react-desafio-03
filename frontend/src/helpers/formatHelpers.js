const formatter = Intl.NumberFormat('pt-BR');

function formatNumber(value) {
  return formatter.format(value);
}

function formatPercentage(value) {
  if (!value) {
    return '0%';
  }

  const stringValue = value.toFixed(2);

  return stringValue.replace('.', ',') + '%';
}

export { formatNumber, formatPercentage };
