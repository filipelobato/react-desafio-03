import React from 'react';

export default function Position({ children }) {
  return <div style={{ marginRight: '5px' }}>{children}</div>;
}
