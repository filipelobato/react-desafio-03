import React from 'react';
import CountUp from 'react-countup';

export default function Percentage({ value, previous }) {
  return (
    <div>
      <CountUp
        start={previous || 0}
        end={value}
        duration={0.6}
        suffix="%"
        decimal=","
        decimals={2}
      >
        {({ countUpRef, start }) => (
          <div>
            <span ref={countUpRef} />
          </div>
        )}
      </CountUp>
    </div>
  );
}
